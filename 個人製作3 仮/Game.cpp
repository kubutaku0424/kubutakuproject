#include "Game.h"
#include "SceneMgr.h"
#include "DxLib.h"
#include "Chara.h"
#include "Player1.h"
#include "Background_Game.h"
#include "Player1_move.h"



//プレイヤー1のスタンプを押した状態
player1 sulime;

//player2 gobulin;


//初期化
void Game_Initialize() {

    Background_Game_Initialize();//初期化

    sulime.player1::player1();
    Player1_move_Initialize();

}

//終了処理
void Game_Finalize() {
}

//更新
void Game_Update() {
    if (CheckHitKey(KEY_INPUT_ESCAPE) != 0) { //Escキーが押されていたら
        SceneMgr_ChangeScene(eScene_Menu);//シーンをメニューに変更
    }
    Background_Game_Update();
    Player1_move_Update();
 
}

//描画
void Game_Draw() {
    Background_Game_Draw();//描画
    DrawString(0, 0, "ゲーム画面です。", GetColor(255, 255, 255));
    DrawString(0, 20, "Escキーを押すとメニュー画面に戻ります。", GetColor(255, 255, 255));
    //sulime.player1_Draw();
    Player1_move_Draw();
    
    
}